showmethebubbles = {}

-- temp vars
local showBattlegroupChatBubbles = true
local showShoutChatBubbles = true

function showmethebubbles.Initialize()
	showmethebubbles.chatMenuHooked = false

	if not showmethebubbles.settings then
		showmethebubbles.settings = {}
		showmethebubbles.settings.version = "1.0.0"
		showmethebubbles.settings.showSettingInChatMenu = true
		showmethebubbles.settings.showWarbandBubbles = true
		showmethebubbles.settings.showShoutBubbles = true
		showmethebubbles.settings.showTellBubbles = true
		showmethebubbles.settings.showScenarioBubbles = true
		showmethebubbles.settings.showScenarioPartyBubbles = true
		showmethebubbles.settings.showRegionBubbles = true
		showmethebubbles.settings.showRvrBubbles = true
	end
	
	-- hook
	showmethebubbles.hookChatMenu(true)
	-- REGISTER EVENT HANDLERS
	RegisterEventHandler( SystemData.Events.CHAT_TEXT_ARRIVED, "showmethebubbles.OnChatText")
end

function showmethebubbles.OnChatText() -- ChatManager.OnChatText()
	--d(GameData.ChatData.text)

	-- show the bubble
    if( (GameData.ChatData.type == SystemData.ChatLogFilters.BATTLEGROUP 		and showmethebubbles.settings.showWarbandBubbles)
    	or (GameData.ChatData.type == SystemData.ChatLogFilters.SHOUT 			and showmethebubbles.settings.showShoutBubbles) 
    	or (GameData.ChatData.type == SystemData.ChatLogFilters.TELL_RECEIVE 	and showmethebubbles.settings.showTellBubbles) 
    	or (GameData.ChatData.type == SystemData.ChatLogFilters.TELL_SEND 		and showmethebubbles.settings.showTellBubbles) 
    	or (GameData.ChatData.type == SystemData.ChatLogFilters.SCENARIO 		and showmethebubbles.settings.showScenarioBubbles) 
    	or (GameData.ChatData.type == SystemData.ChatLogFilters.SCENARIO_GROUPS and showmethebubbles.settings.showScenarioPartyBubbles) 
    	or (GameData.ChatData.type == SystemData.ChatLogFilters.CHANNEL_1		and showmethebubbles.settings.showRegionBubbles)
    	or (GameData.ChatData.type == SystemData.ChatLogFilters.CHANNEL_2		and showmethebubbles.settings.showRvrBubbles)
    	--or GameData.ChatData.type == SystemData.ChatLogFilters.ZONE_AREA
        --and ( GameData.ChatData.objectId ~= GameData.Player.worldObjNum or GameData.ChatData.type == SystemData.ChatLogFilters.SAY  ) 
        )
    then
        ChatManager.AddChatText( GameData.ChatData.objectId, GameData.ChatData.text )    
    end
end


function showmethebubbles.hookChatMenu(enable)
	if enable and not showmethebubbles.chatMenuHooked then
		-- set hook
		showmethebubbles.OnOpenChatMenuHook	= EA_ChatWindow.OnOpenChatMenu
		EA_ChatWindow.OnOpenChatMenu 	= showmethebubbles.OnOpenChatMenu
		showmethebubbles.chatMenuHooked = true

	elseif showmethebubbles.chatMenuHooked then
		-- unhook
		EA_ChatWindow.OnOpenChatMenu = showmethebubbles.OnOpenChatMenuHook
		showmethebubbles.chatMenuHooked = false
	end
end

function showmethebubbles.OnOpenChatMenu(...)
	showmethebubbles.OnOpenChatMenuHook(...)
	local menuItemsAdded = false
	
	if( showmethebubbles.settings.showSettingInChatMenu ) then
		EA_Window_ContextMenu.AddCascadingMenuItem(L"Additional speech bubbles", showmethebubbles.SpawnOptionsMenu, false, EA_Window_ContextMenu.CONTEXT_MENU_1)
		menuItemsAdded = true
	end	
	
	if( menuItemsAdded ) then
		EA_Window_ContextMenu.Finalize(EA_Window_ContextMenu.CONTEXT_MENU_1)
	end
end

function showmethebubbles.SpawnOptionsMenu()

    EA_Window_ContextMenu.Hide(EA_Window_ContextMenu.CONTEXT_MENU_3)
    EA_Window_ContextMenu.CreateContextMenu("", EA_Window_ContextMenu.CONTEXT_MENU_2)

    local function toggleText(state)
    	if state then
    		return L"[o] "
    	else
    		return L"[ ] "
    	end
    end

    EA_Window_ContextMenu.AddMenuItem(toggleText(showmethebubbles.settings.showWarbandBubbles)..L"Warband", 
    	function () showmethebubbles.settings.showWarbandBubbles = not showmethebubbles.settings.showWarbandBubbles end, false, true, EA_Window_ContextMenu.CONTEXT_MENU_2)
    
    EA_Window_ContextMenu.AddMenuItem(toggleText(showmethebubbles.settings.showShoutBubbles)..L"Shout", 
    	function () showmethebubbles.settings.showShoutBubbles = not showmethebubbles.settings.showShoutBubbles end, false, true, EA_Window_ContextMenu.CONTEXT_MENU_2)
    
    EA_Window_ContextMenu.AddMenuItem(toggleText(showmethebubbles.settings.showTellBubbles)..L"Tell", 
    	function () showmethebubbles.settings.showTellBubbles = not showmethebubbles.settings.showTellBubbles end, false, true, EA_Window_ContextMenu.CONTEXT_MENU_2)
    
    EA_Window_ContextMenu.AddMenuItem(toggleText(showmethebubbles.settings.showScenarioBubbles)..L"Scenario", 
    	function () showmethebubbles.settings.showScenarioBubbles = not showmethebubbles.settings.showScenarioBubbles end, false, true, EA_Window_ContextMenu.CONTEXT_MENU_2)
    
    EA_Window_ContextMenu.AddMenuItem(toggleText(showmethebubbles.settings.showScenarioPartyBubbles)..L"Scenario party", 
    	function () showmethebubbles.settings.showScenarioPartyBubbles = not showmethebubbles.settings.showScenarioPartyBubbles end, false, true, EA_Window_ContextMenu.CONTEXT_MENU_2)

    EA_Window_ContextMenu.AddMenuItem(toggleText(showmethebubbles.settings.showRegionBubbles)..L"Region", 
    	function () showmethebubbles.settings.showRegionBubbles = not showmethebubbles.settings.showRegionBubbles end, false, true, EA_Window_ContextMenu.CONTEXT_MENU_2)

    EA_Window_ContextMenu.AddMenuItem(toggleText(showmethebubbles.settings.showRvrBubbles)..L"Region RVR", 
    	function () showmethebubbles.settings.showRvrBubbles = not showmethebubbles.settings.showRvrBubbles end, false, true, EA_Window_ContextMenu.CONTEXT_MENU_2)
    
    EA_Window_ContextMenu.Finalize(EA_Window_ContextMenu.CONTEXT_MENU_2)
end